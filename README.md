opad
====
3D



maven -U deploy

Please adapt that for Maven 3 
to run the project:

%JAVA_HOME%\bin\java" \
-Dmaven.multiModuleProjectDirectory=C:\Users\Win\IdeaProjects\opad \
"-Dmaven.home=C:\Program Files\JetBrains\IntelliJ IDEA 2017.1.6\plugins\maven\lib\maven3" "-Dclassworlds.conf=C:\Program Files\JetBrains\IntelliJ IDEA 2017.1.6\plugins\maven\lib\maven3\bin\m2.conf" -Dfile.encoding=UTF-8 -classpath "C:\Program Files\JetBrains\IntelliJ IDEA 2017.1.6\plugins\maven\lib\maven3\boot\plexus-classworlds-2.5.2.jar" org.codehaus.classworlds.Launcher -Didea.version=2017.1.6 \
--non-recursive --fail-fast -U deploy -P env-devel

Main client class: "be.manudahmen.apps.opad.ServerMain"
