/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.manudahmen.apps.opad;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Win on 28-10-18.
 */
public class ServerMain {
    private int portReceiveLocal;
    private int portInstanceServer;

    private ServerMain(int portReceiveLocal, int portInstanceServer) {
        this.portReceiveLocal = portReceiveLocal;
        this.portInstanceServer = portInstanceServer;
    }

    public static void main(String... args) {
        ServerMain serverMain;

        if (args.length >= 2) {
            serverMain = new ServerMain(Integer.parseInt(args[0]),
                    Integer.parseInt(args[1]));
        } else {
            serverMain = new ServerMain(1977578,
                    -1);
        }
    }

    public void listenAndDispatch() {
        ServerSocket sock = null;
        try {
            sock = new ServerSocket(portInstanceServer);
        } catch (IOException ioe) {
            System.err.println(ioe);
        }

        // now listen for connections
        while (true && sock != null) {

            Socket client = null;
            try {
                client = sock.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // we have a connection


            String readRequest = Server.readRequest(client);

            Server server = null;


            Server serverChoice = ServerChoice.getServer(readRequest);

            server.communicate(client);

            // close the socket and resume listening for more connections
            server.closeSocket();
        }

    }
}
