/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.manudahmen.apps.opad;


import one.empty3.apps.pad.GamePanel;
import one.empty3.apps.pad.Player;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Win on 31-10-18.
 */
public class Game {
    private List<Player> allPlayers;
    private HashMap<Player, GamePanel> maps;

    public Game(List<Player> allPlayers, HashMap<Player, GamePanel> maps) {
        this.allPlayers = allPlayers;
        this.maps = maps;
    }

    public List<Player> getAllPlayers() {
        return allPlayers;
    }

    public void setAllPlayers(List<Player> allPlayers) {
        this.allPlayers = allPlayers;
    }

    public HashMap<Player, GamePanel> getMaps() {
        return maps;
    }

    public void setMaps(HashMap<Player, GamePanel> maps) {
        this.maps = maps;
    }
}
