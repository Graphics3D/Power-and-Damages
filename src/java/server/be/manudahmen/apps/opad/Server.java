/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.manudahmen.apps.opad;


import java.io.*;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by Win on 29-10-18.
 */
public abstract class Server {
    private Socket socket = null;

    public Server(Socket client) {
    }

    public static String readRequest(Socket socket)
    {
        String commandServer = "";
        try {
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();


            InputStreamReader reader = new InputStreamReader(inputStream);

            StringBuffer stringBuffer = new StringBuffer(1024);

            char[] chars = new char[1024];
            int nRead = 0;
            while((nRead = reader.read(chars))>0)
            {
                stringBuffer.append(Arrays.copyOf(chars, nRead));
            }

            commandServer = stringBuffer.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }


        return commandServer;
    }

    public static void response(Socket socket, String responseText)
    {
        PrintStream printStream = null;
        try {
            OutputStream outputStream = socket.getOutputStream();

            printStream = new PrintStream(outputStream);

            printStream.println(responseText);
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }



    }

    public void communicate(Socket socket)
    {
        String request = readRequest(socket);

        Server serverChoice = ServerChoice.getServer(request);

        String response = "";

        if(serverChoice instanceof Server)
        {
        response = serverChoice.evaluate(serverChoice);

        }


        response(socket, response);
    }

    protected abstract String evaluate(Server serverChoice);


    public void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
