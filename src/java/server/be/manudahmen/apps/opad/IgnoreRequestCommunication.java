/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.manudahmen.apps.opad;

import java.net.Socket;

/**
 * Created by Win on 06-11-18.
 */
public class IgnoreRequestCommunication extends Server {
    public IgnoreRequestCommunication(Socket client) {
        super(client);
    }

    @Override
    protected String evaluate(Server serverChoice) {
        return null;
    }


    protected String evaluate(String request) {
        return "";
    }
}
