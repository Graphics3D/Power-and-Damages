/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package one.empty3.apps.pad;

import one.empty3.library.Point2D;
import one.empty3.library.Point3D;

/**
 * Created by manuel on 28-08-16.
 */
public class GamePositionImpl extends Thread implements GamePosition
{
    public void start() {

    }

    @Override
    public Point3D position() {
        return null;
    }

    @Override
    public Point2D positionCard() {
        return null;
    }
}
