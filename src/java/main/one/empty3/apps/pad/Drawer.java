/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package one.empty3.apps.pad;

import one.empty3.library.LineSegment;
import one.empty3.library.Point2D;
import one.empty3.apps.pad.menu.ToggleMenu;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public abstract class Drawer {


    protected ToggleMenu toggleMenu;
    protected Plotter3D plotter3D;
    private Class level;

    public abstract void setLogic(PositionUpdate l);

    public void initFrame(JFrame component) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        JFrame.setDefaultLookAndFeelDecorated(true);

        JMenuBar jMenuBar = new JMenuBar();

        jMenuBar.add(new JMenu("Game"));

        component.setJMenuBar(jMenuBar);

        screenSize.setSize(screenSize.getWidth() - 100, screenSize.getHeight() - 100);

        component.setSize(screenSize);
        Image image = Toolkit.getDefaultToolkit().getImage("favicon.ico");
        component.setIconImage(image);
        ArrayList<Image> imageList = new ArrayList<Image>();
        imageList.add(image);
        component.setIconImages(imageList);

        

        
    }

    /**
     *
     * @param p Point 2D in the window (mouse cordinates)
     * @return Segment Near Far direction of click
     */
    public abstract LineSegment click(Point2D p);

    public void setToggleMenu(ToggleMenu toggleMenu) {
        this.toggleMenu = toggleMenu;
    }

    public void setPlotter3D(Plotter3D plotter3D) {
        this.plotter3D  = plotter3D;
    }

    public Class getLevel() {
        return level;
    }

    public void setLevel(Class level) {
        this.level = level;
    }

}
