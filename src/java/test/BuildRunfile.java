/*
 *     3D game
 *     Copyright (C) 2010-2017  Manuel DAHMEN
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class BuildRunfile extends Thread {
    public static String[] pArgs;
    public static String cmd, cmd0;
    public static String target;
    public static String argClasspath;
    public static String argNatives;
    public static String classToRun;
    public static String batName;
    public static String jarsFolder;

    public static void main(String[] args) {

        pArgs = args;
        new BuildRunfile().start();
    }

    public static void iterateDirectory(File dir) throws IOException {
        if (dir.isDirectory()) {
            String[] list = dir.list();
            for (String fileStr : list
                    ) {
                File file = new File(dir.getCanonicalFile() + "\\" + fileStr);
                if (!file.exists()) {
                    throw new FileNotFoundException("not found= " + file.getCanonicalPath());
                }
                if (fileStr != "" && fileStr != "..") {
                    if (file.isDirectory()) {
                        argClasspath += ";" + file.getCanonicalPath() + "\\*";
                        argNatives += ";" + dir.getCanonicalPath() + "\\" + fileStr;
                        iterateDirectory(file);
                    }
                }
            }
        }
    }

    public void run() {
        String[] args = pArgs;

        ResourceBundle bundle = ResourceBundle.getBundle("BundleRunfile");

        cmd0 = "%JAVA_HOME%\\bin\\java";
        cmd = bundle.getString("java_home") + "\\bin\\java";


        target = bundle.getString("target");
        classToRun = bundle.getString("classToRun");
        batName = bundle.getString("batName");
        String nativePath = bundle.getString("argNatives");
        jarsFolder = nativePath;
        if (args.length >= 1 && args[0] != null) {
            classToRun = args[0];
        }
        if (args.length >= 2 && args[1] != null) {
            batName = args[1];
        }
        if (args.length >= 3 && args[2] != null) {
            jarsFolder = args[2];
            nativePath = args[2];
        }

        System.out.println("Argument: {0} class to run at startup (main class) Actual:" + classToRun);
        System.out.println("Argument: {1} batch file name. Assuming: Actual" + batName);
        System.out.println("Argument: {2} Libs (natives and java) folder. Assuming: " + jarsFolder + " (jars) " + nativePath + " (natives libs - jni)");
        System.out.println("Argument: {3} Application Jar. Assuming: " + target);

        argClasspath = "-classpath \".;" + target + ";lib\\*";
        argNatives = "-Djava.library.path=\".;" + nativePath;


        jarsFolder = ".\\lib";
        File dir = new File(jarsFolder);


        try {
            iterateDirectory(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String fileBat0 = "cd "+bundle.getString("workingDirectory")+"\nset JAVA_HOME="+cmd0 +"\n\"" + cmd0 + "\"" + " " + argClasspath + "\" " + argNatives + "\" " + classToRun;
        String fileBat = "cd "+bundle.getString("workingDirectory")+"\nset JAVA_HOME="+cmd +"\n\""+ cmd + "\""+" " + argClasspath + "\" " + argNatives + "\" " + classToRun;

        try {
            File f = new File(batName);
            System.out.println("Writing file  = " + f.getAbsolutePath()) ;
            if(!f.exists())
                f.createNewFile();
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(f));

            PrintWriter printWriter = new PrintWriter(bufferedOutputStream);
            printWriter.append(fileBat0 + "\n");
            printWriter.append(fileBat + "\n");
            printWriter.close();
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
